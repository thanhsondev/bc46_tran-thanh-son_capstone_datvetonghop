import React, { useState } from "react";
import {NavLink, Outlet, useNavigate} from 'react-router-dom'
import {
  FolderAddOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  VideoCameraAddOutlined,
} from "@ant-design/icons";
import type { MenuProps } from "antd";
import { Breadcrumb, Layout, Menu, theme } from "antd";
import { PATH } from "constant";

const { Header, Content, Sider } = Layout;


type MenuItem = Required<MenuProps>["items"][number];

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[]
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
  } as MenuItem;
}

const items: MenuItem[] = [
 
  getItem("Người dùng", "sub1", <NavLink to={PATH.user}><UserOutlined /></NavLink>),
  getItem("Phim", "sub2", <MenuUnfoldOutlined />, [
    getItem("Danh sách phim", "sub3", <NavLink to={PATH.film}><VideoCameraAddOutlined /></NavLink>),
    getItem("Thêm phim", "sub4", <NavLink to={PATH.addfilm}><FolderAddOutlined /></NavLink>),
    getItem("Chỉnh sửa phim", "sub5", <NavLink to={PATH.editfilm}><FolderAddOutlined /></NavLink>),

  ]),
  getItem("Showtime", "9"),
];

export const AdminTemplate = () => {
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const navigate = useNavigate()
  return (
    <div>
      <Layout style={{ minHeight: "100vh" }}>
        <Sider
          collapsible
          collapsed={collapsed}
          onCollapse={(value) => setCollapsed(value)}
        >
          <div className="demo-logo-vertical text-white text-center font-bold text-2xl pt-4 pb-4">
            <p onClick={()=> {
              navigate('/')
            }} className="cursor-pointer ">GLC Movie</p>
          </div>
          <Menu
            theme="dark"
            defaultSelectedKeys={["1"]}
            mode="inline"
            items={items}
          />
        </Sider>
        <Layout>
          <Header style={{ padding: 0, background: colorBgContainer }}>
            <div onClick={()=> {
              navigate('/')
            }} className="text-right px-10 text-blue-500 cursor-pointer">
              Đăng xuất
            </div>
          </Header>
          <Content style={{ margin: "0 16px" }}>
            <Breadcrumb style={{ margin: "16px 0" }}></Breadcrumb>
            <div
              style={{
                padding: 24,
                minHeight: 360,
                background: colorBgContainer,
              }}
            >
              <Outlet/>
            </div>
          </Content>
        </Layout>
      </Layout>
    </div>
  );
};

export default AdminTemplate;
