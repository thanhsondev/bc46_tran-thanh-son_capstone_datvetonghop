import { DatePicker, Form, Input, InputNumber, Switch } from "antd";
import { useFormik } from "formik";
import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { RootState, useAppDispatch } from "store";
import { editMovieThunk, updateMovieThunk } from "store/quanLyPhim/thunk";
import dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat";

// import moment from "moment";

dayjs.extend(customParseFormat);

export const EditFilmTemplate = () => {
  const [imgSrc, setImgSrc] = useState("");
  const dispatch = useAppDispatch();
  const { formEdit } = useSelector(
    (state: RootState) => state.quanLyPhimReducer
  );

  const params = useParams();
  useEffect(() => {
    const { id } = params;
    dispatch(editMovieThunk(id));
  }, [dispatch]);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      maPhim: formEdit?.maPhim,
      tenPhim: formEdit?.tenPhim,
      trailer: formEdit?.trailer,
      moTa: formEdit?.moTa,
      maNhom: "GP00",
      ngayKhoiChieu: formEdit?.ngayKhoiChieu,
      dangChieu: formEdit?.dangChieu,
      sapChieu: formEdit?.sapChieu,
      hot: formEdit?.hot,
      danhGia: formEdit?.danhGia,
      hinhAnh: {},
    },

    onSubmit: (values: any) => {
      console.log("value: ", values);

      //Tạo đối tượng data
      const formData = new FormData();
      for (let key in values) {
        if (key !== "hinhAnh") {
          formData.append(key, values[key]);
        } else {
          if (values.hinhAnh instanceof File) {
            formData.append("File", values.hinhAnh, values.hinhAnh.name);
          }
        }
      }
      dispatch(updateMovieThunk(formData));
      toast.success("Cập nhật thành công");
    },
  });

  const handleChangeDataPicker = (value: any) => {
    let ngayKhoiChieu = dayjs(value);
    formik.setFieldValue("ngayKhoiChieu", ngayKhoiChieu);
  };

  const handleChangeSwitch = (name: any) => {
    return (value: any) => {
      formik.setFieldValue(name, value);
    };
  };

  const handlleChangeInputNumber = (name: any) => {
    return (value: any) => {
      formik.setFieldValue(name, value);
    };
  };

  const handleChangeFile = (e: any) => {
    let file = e.target.files[0];
    if (
      file.type === "image/png" ||
      file.type === "image/jpeg" ||
      file.type === " image/gif"
    ) {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (e: any) => {
        setImgSrc(e.target.result); //hình base64
      };
    }
    formik.setFieldValue("hinhAnh", file);
  };

  return (
    <Form
      onSubmitCapture={formik.handleSubmit}
      labelCol={{ span: 4 }}
      wrapperCol={{ span: 14 }}
      layout="horizontal"
      style={{ maxWidth: 600 }}
    >
      <Form.Item label="Tên Phim">
        <Input
          name="tenPhim"
          onChange={formik.handleChange}
          value={formik.values.tenPhim}
        />
      </Form.Item>

      <Form.Item label="Trailer">
        <Input
          name="trailer"
          onChange={formik.handleChange}
          value={formik.values.trailer}
        />
      </Form.Item>
      <Form.Item label="Mô tả">
        <Input
          name="moTa"
          onChange={formik.handleChange}
          value={formik.values.moTa}
        />
      </Form.Item>
      <Form.Item label="Ngày khởi chiếu ">
        <DatePicker
          onChange={handleChangeDataPicker}
          format={"DD/MM/YYYY"}
          value={dayjs(formik.values.ngayKhoiChieu)}
        />
      </Form.Item>
      <Form.Item label="Đang chiếu">
        <Switch
          onChange={handleChangeSwitch("dangChieu")}
          checked={formik.values.dangChieu}
        />
      </Form.Item>
      <Form.Item label="Sắp chiếu">
        <Switch
          onChange={handleChangeSwitch("sapChieu")}
          checked={formik.values.sapChieu}
        />
      </Form.Item>
      <Form.Item label="Hot">
        <Switch
          onChange={handleChangeSwitch("hot")}
          checked={formik.values.hot}
        />
      </Form.Item>
      <Form.Item label="Số sao">
        <InputNumber
          onChange={handlleChangeInputNumber("danhGia")}
          min={1}
          max={10}
          value={formik.values.danhGia}
        />
      </Form.Item>
      <Form.Item label="Hình ảnh">
        <input
          type="file"
          onChange={handleChangeFile}
          accept="image/png, image/jpeg, image/gif"
        />
        <br />
        <img
          style={{ width: "150", height: "150px" }}
          alt="..."
          src={imgSrc === "" ? formEdit?.hinhAnh : imgSrc}
        />
      </Form.Item>

      <Form.Item label="Thao tác">
        <button type="submit" className="bg-blue-500 text-white rounded-md p-2">
          Cập nhật
        </button>
      </Form.Item>
    </Form>
  );
};

export default EditFilmTemplate;
