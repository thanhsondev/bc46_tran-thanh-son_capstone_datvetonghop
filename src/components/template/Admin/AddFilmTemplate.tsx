import { DatePicker, Form, Input, InputNumber, Switch } from "antd";
import { useFormik } from "formik";
import moment from "moment";
import { useState } from "react";
import { toast } from "react-toastify";
import { useAppDispatch } from "store";
import { addMovieThunk } from "store/quanLyPhim/thunk";

export const AddFilmTemplate = () => {
  const [imgSrc, setImgSrc] = useState("");
  const dispatch = useAppDispatch()

  const formik = useFormik({
    initialValues: {
      tenPhim: "",
      trailer: "",
      moTa: "",
      ngayKhoiChieu: "",
      dangChieu: false,
      sapChieu: false,
      hot: false,
      danhGia: 0,
      hinhAnh: {},
    },
    onSubmit: (values: any) => {
      console.log("value: ", values);
      //Tạo đối tượng data
      let formData = new FormData()
      for(let key in values) {
        if(key !== "hinhAnh") {
          formData.append(key, values[key])
        } else {
          if(values.hinhAnh instanceof File) {
            formData.append('file', values.hinhAnh, values.hinhAnh.name)
          }
        }
      }
      dispatch(addMovieThunk(formData))
      toast.success('Thêm thành công')
    },
  });

  const handleChangeDataPicker = (value: any) => {
    let ngayKhoiChieu = moment(value).format("DD/MM/yyyy");

    formik.setFieldValue("ngayKhoiChieu", ngayKhoiChieu);
  };

  const handleChangeSwitch = (name: any) => {
    return (value: any) => {
      formik.setFieldValue(name, value);
    };
  };

  const handlleChangeInputNumber = (name: any) => {
    return (value: any) => {
      formik.setFieldValue(name, value);
    };
  };

  const handleChangeFile = (e: any) => {
    let file = e.target.files[0];
    if (
      file.type === "image/png" ||
      file.type === "image/jpeg" ||
      file.type === " image/gif"
    ) {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (e: any) => {
        setImgSrc(e.target.result); //hình base64
      };

    }
    formik.setFieldValue("hinhAnh", file)
  };

  return (
    <Form
      onSubmitCapture={formik.handleSubmit}
      labelCol={{ span: 4 }}
      wrapperCol={{ span: 14 }}
      layout="horizontal"
      style={{ maxWidth: 600 }}
    >
      <Form.Item label="Tên Phim">
        <Input name="tenPhim" onChange={formik.handleChange} />
      </Form.Item>

      <Form.Item label="Trailer">
        <Input name="trailer" onChange={formik.handleChange} />
      </Form.Item>
      <Form.Item label="Mô tả">
        <Input name="moTa" onChange={formik.handleChange} />
      </Form.Item>
      <Form.Item label="Ngày khởi chiếu ">
        <DatePicker format={"DD/MM/YYYY"} onChange={handleChangeDataPicker} />
      </Form.Item>
      <Form.Item label="Đang chiếu">
        <Switch onChange={handleChangeSwitch("dangChieu")} />
      </Form.Item>
      <Form.Item label="Sắp chiếu">
        <Switch onChange={handleChangeSwitch("sapChieu")} />
      </Form.Item>
      <Form.Item label="Hot">
        <Switch onChange={handleChangeSwitch("hot")} />
      </Form.Item>
      <Form.Item label="Số sao">
        <InputNumber
          onChange={handlleChangeInputNumber("danhGia")}
          min={1}
          max={10}
        />
      </Form.Item>
      <Form.Item label="Hình ảnh">
        <input
          type="file"
          onChange={handleChangeFile}
          accept="image/png, image/jpeg, image/gif"
        />
        <br />
        <img style={{ width: "150", height: "150px" }} src={imgSrc} alt="..." />
      </Form.Item>

      <Form.Item label="Thao tác">
        <button type="submit" className="bg-blue-500 text-white rounded-md p-2">
          Thêm phim
        </button>
      </Form.Item>
    </Form>
  );
};

export default AddFilmTemplate;
