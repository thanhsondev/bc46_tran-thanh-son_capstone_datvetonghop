import { Fragment, useEffect } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { RootState, useAppDispatch } from "store";
import { getBookingThunk, thongTinDatVeThunk} from "store/quanLyDatVe/thunk";
import { CloseOutlined } from "@ant-design/icons";
import { quanLyDatVeActions } from "store/quanLyDatVe/slice";
import { toast } from "react-toastify";

const TicketBookingTemplate = () => {
  const dispatch = useAppDispatch();
  const { booking, danhSachGheDangDat, thongTinDatVe } = useSelector(
    (state: RootState) => state.quanLyDatVeReducer
    );
    // console.log('thongTinDatVe: ', thongTinDatVe);
  // console.log("danhSachGheDangDat: ", danhSachGheDangDat);
  // console.log("booking: ", booking);
  const { thongTinPhim, danhSachGhe }: any = booking;

  const params: any = useParams();
  // console.log('params: ', params);

  useEffect(() => {
    let { id} = params;
    dispatch(getBookingThunk(id),);
    
  }, [dispatch]);

  const renderSeats = () => {
    return danhSachGhe?.map((ghe: any, index: any) => {
      let classGheVip = ghe.loaiGhe === "Vip" ? "gheVip" : "";
      let classGheDaDat = ghe.daDat === true ? "gheDaDat" : "";
      let classGheDangDat = "";
      let indexGheDD = danhSachGheDangDat.findIndex(
        (e: any) => e.maGhe === ghe.maGhe
      );
      if (indexGheDD != -1) {
        classGheDaDat = "gheDangDat";
      }
      return (
        <Fragment key={index}>
          {
            <button
              onClick={() => {
                dispatch(quanLyDatVeActions.DAT_VE(ghe));
              }}
              disabled={ghe.daDat}
              className={`${classGheVip} ${classGheDaDat} ${classGheDangDat} ghe hover:bg-slate-800`}
            >
              {ghe.daDat ? <CloseOutlined /> : ghe.stt}
            </button>
          }
        </Fragment>
      );
    });
  };

  return (
    <div className=" datVe ">
      <div className="grid grid-cols-12 mx-auto w-full max-w-screen-xl">
        <div className="col-span-8">
          <p className="text-yellow-600 text-4xl text-center font-bold py-10">
            Trang đặt vé
          </p>
          <div className="trapezoid text-white text-center">Màn hình</div>
          <div>{renderSeats()}</div>
        </div>
        <div className="col-span-4 min-h-full">
          <p className="text-yellow-600 text-4xl text-center font-bold py-10">
            Thông tin đặt vé
          </p>
          <div className="ghiChuGhe">
            <button className="w-full bg-yellow-400 text-white">
              Ghế đã chọn
            </button>
            <button className="bg-green-500 text-white w-full">
              Ghế đang chọn
            </button>
            <button className="bg-slate-400 border-red-200 border-current text-white w-full">
              Ghế trống
            </button>
            <button className="bg-red-500 border-red-200 border-current text-white w-full">
              Ghế trống VIP
            </button>
            <div></div>
          </div>
          <div className="pt-5 text-white">
          
            <hr />
            <h3 className="text-xl pt-2 ">
              {" "}
              Tên Phim: {thongTinPhim?.tenPhim}
            </h3>
            <p className="py-5">Địa chỉ: {thongTinPhim?.diaChi}</p>
            <p className="pb-5">
              Thời gian: {thongTinPhim?.gioChieu} - Ngày:{" "}
              {thongTinPhim?.ngayChieu}{" "}
            </p>
            <hr />
            <div className="">
              <div className="w-4/5 py-2">
                <span>Ghế:</span>
                {danhSachGheDangDat.map((gheDD: any, index: any) => {
                  return (
                    <span key={index} className="text-green-500 ml-2">
                      {gheDD.stt}
                    </span>
                  );
                })}
              </div>
              <div className="py-2">
                Tổng tiền:
                <span className="text-green-500 px-2">
                {danhSachGheDangDat.reduce((tongTien: any, giaVe: any) => {
                  return (tongTien += giaVe.giaVe);
                }, 0).toLocaleString()}
                </span>
                đ
              </div>
            </div>
            <hr />
            <hr />
            <div
              className="mb-0 h-full flex flex-col justify-end items-center mt-40"
              style={{ marginBottom: "0" }}
            >
              <div onClick={()=> {
                {
                  const newThongTinDatVe: any = {...thongTinDatVe}
                  
                  newThongTinDatVe!.maLichChieu = params.id,
                  newThongTinDatVe!.danhSachVe = danhSachGheDangDat
                
                  dispatch(thongTinDatVeThunk(newThongTinDatVe))
                  .unwrap()
                  .then(()=> {
                    toast.success('Đặt vé thành công')
                    location.reload()
                  })
                  
                }
                
                
                // console.log('thongTinDatVe: ', thongTinDatVe);
              }} className="bg-green-500 text-white w-full text-center p-2 font-bold hover:bg-green-800">
                ĐẶT VÉ
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TicketBookingTemplate;
