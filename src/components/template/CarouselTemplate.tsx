import { Carousel } from "antd";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { RootState, useAppDispatch } from "store";
import { getBannerMovieThunk } from "store/quanLyPhim/thunk";

const CarouselTemplate = () => {

  const dispatch = useAppDispatch()
  const {bannerMovie} = useSelector((state: RootState)=> state.quanLyPhimReducer)

  useEffect(()=> {
    dispatch(getBannerMovieThunk())
  }, [dispatch])

  const contentStyle: React.CSSProperties = {
    height: "600px",
    // width:"800px",
    color: "#fff",
    lineHeight: "160px",
    textAlign: "center",
    backgroundSize: "100% 110%",
    backgroundRepeat: "no-repeat",
    backgroundPositionX: "",
  };

  return (
    <Carousel autoplay className="pt-24">
      {bannerMovie?.map((banner) => (
        <div key={banner.maBanner}>
          <div
            style={{
              ...contentStyle,
              backgroundImage: `url(${banner.hinhAnh})`,
            }}
          >
            <img
              src={banner.hinhAnh}
              className="w-full"
              alt=""
            />
          </div>
        </div>
      ))}

    </Carousel>
  );
};

export default CarouselTemplate;
