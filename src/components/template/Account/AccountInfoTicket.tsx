import { useAuth } from "hooks";
import moment from "moment";
// import { useEffect } from "react";
// import { useSelector } from "react-redux";
// import { RootState, useAppDispatch } from "store";

// import { getUserThunk } from "store/quanLyNguoiDung/thunk";

const AccountInfoTicket = () => {
  const { user } = useAuth();
  console.log("user: ", user?.thongTinDatVe);
  // const {thongTinNguoiDung} =useSelector((state:RootState)=> state.quanLyNguoiDungReducer)
  // const dispatch = useAppDispatch()

  // useEffect(()=> {
  //     dispatch(getUserThunk())
  // }, [dispatch])

  return (
    <div>
      <section className="text-gray-600 body-font">
        <div className="container px-5 py-10 mx-auto">
          <div className="flex flex-wrap w-full mb-20 flex-col items-center text-center">
            <h1 className="sm:text-3xl text-2xl font-medium title-font mb-2 text-gray-900">
              Lịch sử đặt vé
            </h1>
            <p className="lg:w-1/2 w-full leading-relaxed text-gray-500">
              Cảm ơn quý khách đã đặt vé
            </p>
          </div>
          <div className="flex flex-wrap -m-4">
            {user?.thongTinDatVe?.map((ticket: any, index) => {
              return (
                <div className="xl:w-1/3 md:w-1/2 p-4" key={index}>
                  <div className="border border-gray-200 p-6 rounded-lg flex">
                    <div className="w-10 h-10 inline-flex items-center justify-center rounded-full bg-indigo-100 text-indigo-500 mb-4">
                      <img src={ticket.hinhAnh} alt="" />
                    </div>
                    <div className="ml-2">
                      <h2 className="text-lg text-gray-900 font-medium title-font mb-2">
                        {ticket?.tenPhim}
                      </h2>
                      <p className="leading-relaxed text-base">
                        Giờ chiếu: {moment(ticket?.ngayDat).format("hh:mm")}
                        <br />
                        Ngày chiếu:{" "}
                        {moment(ticket?.ngayDat).format("DD/MM/YYYY")}
                      </p>
                      <div>Địa điểm: {ticket.danhSachGhe[0].tenHeThongRap} 
                      <br />
                       Tại: {ticket.danhSachGhe[0].tenRap} </div>
                    </div>
                  </div>
                  <div></div>
                </div>
              );
            })}
          </div>
        </div>
      </section>
    </div>
  );
};

export default AccountInfoTicket;
