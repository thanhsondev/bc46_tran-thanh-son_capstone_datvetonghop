import { Tabs } from 'antd'
import AccountInfoTab from './AccountInfoTab'
import AccountInfoTicket from './AccountInfoTicket'

export const AccountTemplate = () => {

    return (
        <div className='min-h-screen mx-auto w-full max-w-screen-xl mt-32'>
            <Tabs
                tabPosition="left"
                className="h-full"
                tabBarGutter={-5}
                items={[
                    {
                        label: (
                            <div className="w-[200px] text-left hover:bg-gray-400 hover:text-white rounded-lg transition-all p-2 text-lg text-black">
                                Thông tin tài khoản
                            </div>
                        ),
                        key: 'accountInfo',
                        children: <AccountInfoTab />,
                    },
                    {
                        label: (
                            <div className="w-[200px] text-left hover:bg-gray-400 hover:text-white rounded-lg transition-all p-2 text-lg text-black">
                                Thông tin vé đã đặt
                            </div>
                        ),
                        key: 'ticketInfo',
                        children: <div className="min-h-full"><AccountInfoTicket/></div>,
                    },
                ]}
            />
        </div>
    )
}

export default AccountTemplate
