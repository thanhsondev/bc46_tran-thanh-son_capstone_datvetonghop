const PromotionTemplate = () => {
  return (
    <div className="mx-auto max-w-screen-xl">
      <p className="text-blue-500 text-center text-4xl font-bold mt-32">
        KHUYẾN MẠI
      </p>
      <section className="text-gray-600 body-font overflow-hidden">
        <div className="container px-5 py-24 mx-auto">
          <div className="lg:w-4/5 mx-auto flex flex-wrap">
            <img
              alt="ecommerce"
              className="lg:w-1/2 lg:h-auto h-10 object-cover object-center rounded"
              src="../../../public/image/carousel/carousel1.jpg"
            />
            <div className="lg:w-1/2 w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0">
              <h1 className="text-gray-900 text-3xl title-font font-medium mb-1">
                GLC MOVIE START - THỨ 3 TUẦN CUỐI MỖI THÁNG
              </h1>

              <p className="leading-relaxed">
                CHIẾN BINH ÁO TÍM - CHIẾN THẦN CINESTAR
                <br />
                Các mem hãy Mặc trang phục màu tím (áo, váy, quần, túi xách,
                nón, giày), mua vé xem phim sau đó nhận ngay 01 vé Xem phim
                Voucher C'GREEN.
                <br />
                Chương trình diễn ra liên tục vào các Thứ 3 Tuần cuối trong
                tháng.
                <br />
                Lưu ý: Áp dụng cho cả khách hàng mua Online và Offline
              </p>
            </div>
          </div>
        </div>
      </section>
      <section className="text-gray-600 body-font overflow-hidden">
        <div className="container px-5 py-10 mx-auto">
          <div className="lg:w-4/5 mx-auto flex flex-wrap">
            <img
              alt="ecommerce"
              className="lg:w-1/2 lg:h-auto h-10 object-cover object-center rounded"
              src="../../../public/image/carousel/carousel1.jpg"
            />
            <div className="lg:w-1/2 w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0">
              <h1 className="text-gray-900 text-3xl title-font font-medium mb-1">
                GLC'MONDAY - ƯU ĐÃI THỨ 2
              </h1>

              <p className="leading-relaxed">
                - Giá vé ưu đãi: 45.000 đ/vé 2D và 55.000 đ/vé 3D.
                <br />
                - Thời gian: Áp dụng cho tất cả các suất chiếu ngày Thứ Hai hàng
                tuần.
                <br /> - Lưu ý: Không áp dụng cho các ngày lễ/tết.
              </p>
            </div>
          </div>
        </div>
      </section>
      <section className="text-gray-600 body-font overflow-hidden">
        <div className="container px-5 py-10 mx-auto">
          <div className="lg:w-4/5 mx-auto flex flex-wrap">
            <img
              alt="ecommerce"
              className="lg:w-1/2 lg:h-auto h-10 object-cover object-center rounded"
              src="../../../public/image/carousel/carousel1.jpg"
            />
            <div className="lg:w-1/2 w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0">
              <h1 className="text-gray-900 text-3xl title-font font-medium mb-1">
                GLC'MEMBER - NGÀY HỘI THÀNH VIÊN
              </h1>

              <p className="leading-relaxed">
                Thành Viên Cinestar được áp dụng giá vé ưu đãi, hạng thẻ
                GLC’FRIEND và GLC’VIP.
                <br />
                Thời gian: Thứ Tư hàng tuần
                <br />
                - Giá vé: 45,000 đ/vé 2D và 55,000 đ/ vé 3D
                <br />
                - Giảm 10% giá trị hóa đơn bắp nước cho chủ thẻ C’FRIEND và 15%
                cho chủ thẻ C’VIP.
                <br />
                - Chương trình tích điểm thành viên và các điều kiện thành viên
                khác được áp dụng.
                <br />
                Lưu ý: - Chỉ áp dụng mua trực tiếp tại quầy.
                <br />- Chương trình không giới hạn số vé và số lần giao dịch
                trong thời gian diễn ra.
                <br /> - Không áp dụng cho các ngày lễ/tết.
              </p>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default PromotionTemplate;
