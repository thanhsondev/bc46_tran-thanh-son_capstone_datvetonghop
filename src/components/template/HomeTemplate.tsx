import { Tabs } from "antd";
import ListMovie from "components/ui/ListMovie";
import MultipleRowSLick from "components/ui/MultipleRowSLick";
import moment from "moment";
import HomeCarousel from "pages/HomeCarousel";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { RootState, useAppDispatch } from "store";
import { getCinemaSystemThunk } from "store/quanLyRap/thunk";

const HomeTemplate = () => {
  const dispatch = useAppDispatch();
  const { heThongRap } = useSelector(
    (state: RootState) => state.quanLyRapReducer
  );

  useEffect(() => {
    dispatch(getCinemaSystemThunk());
  }, [dispatch]);

  return (
    <div className="">
      <HomeCarousel/>
      <MultipleRowSLick/>
      <ListMovie/>
    <section className="text-gray-600 body-font mx-auto w-full max-w-screen-xl ">
      <p className="text-[#1b74e4] text-center my-10 text-4xl font-bold">
        Lịch chiếu phim
      </p>
      <Tabs tabPosition="left" className=" ml-11">
        {heThongRap?.map((heThongRap: any) => {
          return (
            <Tabs.TabPane
              tab={<img height={40} width={40} src={heThongRap.logo}></img>}
              key={heThongRap.maHeThongRap}
            >
              <Tabs tabPosition="left">
                {heThongRap?.lstCumRap?.slice(0, 5).map((cumRap: any) => {
                  return (
                    <Tabs.TabPane
                      tab={
                        <div className="flex">
                          <img height={30} width={30} src={cumRap.hinhAnh} />
                          <div className="ml-3 ">
                            <p className="text-blue-500"> {cumRap.tenCumRap}</p>
                           
                            <p className="text-white text-left">
                              Chi tiết
                            </p>
                          </div>
                        </div>
                      }
                      key={cumRap.maCumRap}
                    >
                      {cumRap?.danhSachPhim?.slice(0, 6).map((phim: any) => {
                        return (
                          <div className="flex my-5" key={phim.maPhim}>
                            <img
                              style={{ height: "50px", width: "50px" }}
                              src={phim.hinhAnh}
                              alt={phim.tenPhim}
                            />
                            <hr />
                            <div className="ml-3 text-base text-[#1b74e4]">
                              {phim.tenPhim}
                              <div className="grid grid-cols-6 gap-5">
                                {phim?.lstLichChieuTheoPhim
                                  ?.slice(0, 12)
                                  .map((lichChieu: any) => {
                                    return (
                                      <div
                                        className="text-white"
                                        key={lichChieu.maLichChieu}
                                      >
                                        {moment(
                                          lichChieu.ngayChieuGioChieu
                                        ).format("HH:mm:A")}
                                      </div>
                                    );
                                  })}
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </Tabs.TabPane>
                  );
                })}
              </Tabs>
            </Tabs.TabPane>
          );
        })}
      </Tabs>
    </section>
    </div>
  );
};

export default HomeTemplate;
