import { UserOutlined } from "@ant-design/icons";
import { Avatar, Popover } from "antd";
import { PATH } from "constant";
import { useAuth } from "hooks";
import { useNavigate } from "react-router-dom";
import { useAppDispatch } from "store";
import { quanLyNguoiDungActions } from "store/quanLyNguoiDung/slice";

export const Header = () => {
  const navigate = useNavigate();
  const { user } = useAuth();
  const dispatch = useAppDispatch()
  return (
    // bg-gradient-to-r from-black via-[#39bcb4] to-black
    <header className="p-4 text-white backdrop-blur-sm bg-black/30 z-30 top-0 w-full fixed ">
      <div className="container flex justify-between h-16 mx-auto max-w-[1400px]">
        <a
          onClick={() => {
            navigate("/");
          }}
          className="flex items-center p-2 cursor-pointer"
        >
          <p className="text-2xl font-bold  text-white ">Movie</p>
        </a>
        <ul className="items-stretch hidden space-x-3 lg:flex">
          <li className="flex">
            <a

              onClick={()=> {
                navigate('/')
              }}
              rel="noopener noreferrer"
              className="flex items-center px-4 -mb-1 cursor-pointer text-white hover:border-b-2 hover:border-[#1b74e4] font-bold "
            >
              Trang chủ
            </a>
          </li>
          <li className="flex">
            <a onClick={()=> {
              navigate(PATH.news)
            }} className="flex items-center px-4 -mb-1 cursor-pointer text-white hover:border-b-2 hover:border-[#1b74e4] font-bold ">Tin tức</a>
          </li>
          <li className="flex">
            <a onClick={()=> {
              navigate(PATH.promotion)
            }} className="flex items-center px-4 -mb-1 cursor-pointer text-white hover:border-b-2 hover:border-[#1b74e4] font-bold ">Khuyến mãi</a>
          </li>
          <li className="flex">
            <a onClick={()=> {
              navigate(PATH.admin)
            }} className="flex items-center px-4 -mb-1 cursor-pointer text-white hover:border-b-2 hover:border-[#1b74e4] font-bold "> Admin</a>
          </li>
        </ul>
        <div className="items-center flex-shrink-0 hidden lg:flex">

          {user && (
            <Popover
              content={
                <div className="mt-2" title="Title">
                  <p className="mt-2 font-bold p-2 text-base">{user?.hoTen}</p>
                  <hr />
                  <div
                    onClick={() => {
                      navigate(PATH.account);
                    }}
                    className="mt-3 hover:bg-slate-400 cursor-pointer rounded-md p-2 hover:text-white text-base text-[#1b74e4]"
                  >
                    Thông tin tài khoản
                  </div>
                  <div onClick={()=> {
                    dispatch(quanLyNguoiDungActions.logOut())
                  }} className="mt2  hover:bg-slate-400 cursor-pointer rounded-md p-2 hover:text-white text-base text-[#1b74e4]">
                    Đăng xuất
                  </div>
                </div>
              }
            >
              <Avatar
                className="!flex !items-center justify-center"
                size={35}
                icon={<UserOutlined />}
              />
            </Popover>
          )}
          {!user && (
            <div className="flex">
              <p onClick={()=> {
                navigate(PATH.login)
              }} className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded mr-5 cursor-pointer">Đăng nhập</p>
              <p onClick={()=> {
                navigate(PATH.register)
              }} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 border border-blue-700 rounded cursor-pointer">Đăng ký</p>
            </div>
          )}
        </div>
      </div>
    </header>
  );
};

export default Header;
