import { PATH } from "constant";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { generatePath, useNavigate } from "react-router-dom";
import Slider from "react-slick";
import { RootState, useAppDispatch } from "store";
import { quanLyPhimActions } from "store/quanLyPhim/slice";
import { getMovieListThunk } from "store/quanLyPhim/thunk";

function SampleNextArrow(props: { className: any; style: any; onClick: any }) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", color: "black" }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props: { className: any; style: any; onClick: any }) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", color: "black" }}
      onClick={onClick}
    />
  );
}

const MultipleRowSLick = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate()
  const { movieList } = useSelector(
    (state: RootState) => state.quanLyPhimReducer
  );

  useEffect(() => {
    dispatch(getMovieListThunk());
  }, [dispatch]);

  const settings = {
    className: "center",
    centerMode: true,
    infinite: true,
    centerPadding: "60px",
    slidesToShow: 4,
    speed: 500,
    rows: 1,
    slidesPerRow: 1,
    nextArrow: (
      <SampleNextArrow
        className={undefined}
        style={undefined}
        onClick={undefined}
      />
    ),
    prevArrow: (
      <SamplePrevArrow
        className={undefined}
        style={undefined}
        onClick={undefined}
        />
        ),
      };
      
     const renderMovieList = (movieListArr: any) => {

       return <Slider {...settings}>
      {movieListArr?.map((movie: any) => {
        const path = generatePath(PATH.detail, {id: movie.maPhim})
        
        return   <div key={movie.maPhim} className="p-1 lg:w-1/7 ">
        <div className="h-[370px]  bg-opacity-75 px-3 pt-3 rounded-lg overflow-hidden text-center relative mt-10">
          <img
            style={{ height: "270px", width: "100%" }}
            src={movie.hinhAnh}
            alt={movie.hinhAnh}
          />
          <h1 className="title-font sm:text-base text-base font-medium text-white mb-3">
            {movie.tenPhim}
          </h1>
          <div onClick={()=> {
            navigate(path)
          }} className=" w-full h-10 flex items-center justify-center bg-[#1b74e4] text-white hover:bg-[#1b74e4]">
            <p className="font-bold">Đặt vé</p>
          </div>
        </div>
      </div>

     })}
    </Slider>
      
     }

     const {phimDangChieu, phimSapChieu} = useSelector((state:RootState)=> state.quanLyPhimReducer)
     let phimDC = phimDangChieu === true ? 'activeClass' : 'activeNoneClass'
     let phimSC = phimSapChieu === true ? 'activeClass' : 'activeNoneClass'
  return (
    
    <div className="mx-auto w-full max-w-screen-xl mt-10 text-center ">
      <button
        type="button"
        className={` px-8 py-3 mt-5 font-semibold rounded mr-3 ${phimDC}`}
        onClick={() => {
          dispatch(quanLyPhimActions.phimDangChieu());
        }}
        >
        Phim đang chiếu
      </button>
      <button
        className={`px-8 py-3 font-semibold rounded ${phimSC}`}
        onClick={() => {
          dispatch(quanLyPhimActions.phimSapChieu());
        }}
        >
        Phim sắp chiếu
      </button>
        {renderMovieList(movieList)}
    </div>
  );
};

export default MultipleRowSLick;
