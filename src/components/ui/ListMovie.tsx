const ListMovie = () => {
  return (
    <section className="text-gray-600 body-font  my-10 bg-[url('./public/image/backgroud/nenbautroi.jpg')] ">
        <h1 className="text-white text-4xl font-semibold text-center">Danh sách phim</h1>
      <div className="container px-5 py-10 mx-auto max-w-7xl">
        <div className="flex flex-wrap -m-4">
          <div className="xl:w-1/4 md:w-1/2 p-4">
            <div className="bg-[#0b0e12]p-2 rounded-lg">
              <img
                className="h-40 rounded w-full object-cover object-center mb-6"
                src="https://chieuphimquocgia.com.vn/_next/image?url=https%3A%2F%2Fapi.chieuphimquocgia.com.vn%2FContent%2FImages%2F0017064_0.jpg&w=1920&q=75"
                alt="content"
              />
              <h3 className="tracking-widest text-indigo-500 text-xs font-medium title-font">
              Tâm lý tình cảm
              </h3>
              <h2 className="text-lg text-white font-medium title-font mb-4">
                Đất rừng phương nam
              </h2>
              <p className="leading-relaxed text-base">
                Fingerstache flexitarian street art 8-bit waistcoat. Distillery
                hexagon disrupt edison bulbche.
              </p>
            </div>
          </div>
          <div className="xl:w-1/4 md:w-1/2 p-4">
            <div className="bg-[#0b0e12]p-2 rounded-lg">
              <img
                className="h-40 rounded w-full object-cover object-center mb-6"
                src="https://chieuphimquocgia.com.vn/_next/image?url=https%3A%2F%2Fapi.chieuphimquocgia.com.vn%2FContent%2FImages%2F0017064_0.jpg&w=1920&q=75"
                alt="content"
              />
              <h3 className="tracking-widest text-indigo-500 text-xs font-medium title-font">
              Tâm lý tình cảm
              </h3>
              <h2 className="text-lg text-white font-medium title-font mb-4">
                Đất rừng phương nam
              </h2>
              <p className="leading-relaxed text-base">
                Fingerstache flexitarian street art 8-bit waistcoat. Distillery
                hexagon disrupt edison bulbche.
              </p>
            </div>
          </div>
          <div className="xl:w-1/4 md:w-1/2 p-4">
            <div className="bg-[#0b0e12]p-2 rounded-lg">
              <img
                className="h-40 rounded w-full object-cover object-center mb-6"
                src="https://chieuphimquocgia.com.vn/_next/image?url=https%3A%2F%2Fapi.chieuphimquocgia.com.vn%2FContent%2FImages%2F0017064_0.jpg&w=1920&q=75"
                alt="content"
              />
              <h3 className="tracking-widest text-indigo-500 text-xs font-medium title-font">
              Tâm lý tình cảm
              </h3>
              <h2 className="text-lg text-white font-medium title-font mb-4">
                Đất rừng phương nam
              </h2>
              <p className="leading-relaxed text-base">
                Fingerstache flexitarian street art 8-bit waistcoat. Distillery
                hexagon disrupt edison bulbche.
              </p>
            </div>
          </div>
          <div className="xl:w-1/4 md:w-1/2 p-4">
            <div className="bg-[#0b0e12]p-2 rounded-lg">
              <img
                className="h-40 rounded w-full object-cover object-center mb-6"
                src="https://chieuphimquocgia.com.vn/_next/image?url=https%3A%2F%2Fapi.chieuphimquocgia.com.vn%2FContent%2FImages%2F0017064_0.jpg&w=1920&q=75"
                alt="content"
              />
              <h3 className="tracking-widest text-indigo-500 text-xs font-medium title-font">
              Tâm lý tình cảm
              </h3>
              <h2 className="text-lg text-white font-medium title-font mb-4">
                Đất rừng phương nam
              </h2>
              <p className="leading-relaxed text-base">
                Fingerstache flexitarian street art 8-bit waistcoat. Distillery
                hexagon disrupt edison bulbche.
              </p>
            </div>
          </div>
          <div className="xl:w-1/4 md:w-1/2 p-4">
            <div className="bg-[#0b0e12]p-2 rounded-lg">
              <img
                className="h-40 rounded w-full object-cover object-center mb-6"
                src="https://chieuphimquocgia.com.vn/_next/image?url=https%3A%2F%2Fapi.chieuphimquocgia.com.vn%2FContent%2FImages%2F0017064_0.jpg&w=1920&q=75"
                alt="content"
              />
              <h3 className="tracking-widest text-indigo-500 text-xs font-medium title-font">
              Tâm lý tình cảm
              </h3>
              <h2 className="text-lg text-white font-medium title-font mb-4">
                Đất rừng phương nam
              </h2>
              <p className="leading-relaxed text-base">
                Fingerstache flexitarian street art 8-bit waistcoat. Distillery
                hexagon disrupt edison bulbche.
              </p>
            </div>
          </div>
          <div className="xl:w-1/4 md:w-1/2 p-4">
            <div className="bg-[#0b0e12]p-2 rounded-lg">
              <img
                className="h-40 rounded w-full object-cover object-center mb-6"
                src="https://chieuphimquocgia.com.vn/_next/image?url=https%3A%2F%2Fapi.chieuphimquocgia.com.vn%2FContent%2FImages%2F0017064_0.jpg&w=1920&q=75"
                alt="content"
              />
              <h3 className="tracking-widest text-indigo-500 text-xs font-medium title-font">
              Tâm lý tình cảm
              </h3>
              <h2 className="text-lg text-white font-medium title-font mb-4">
                Đất rừng phương nam
              </h2>
              <p className="leading-relaxed text-base">
                Fingerstache flexitarian street art 8-bit waistcoat. Distillery
                hexagon disrupt edison bulbche.
              </p>
            </div>
          </div>
          <div className="xl:w-1/4 md:w-1/2 p-4">
            <div className="bg-[#0b0e12]p-2 rounded-lg">
              <img
                className="h-40 rounded w-full object-cover object-center mb-6"
                src="https://chieuphimquocgia.com.vn/_next/image?url=https%3A%2F%2Fapi.chieuphimquocgia.com.vn%2FContent%2FImages%2F0017064_0.jpg&w=1920&q=75"
                alt="content"
              />
              <h3 className="tracking-widest text-indigo-500 text-xs font-medium title-font">
              Tâm lý tình cảm
              </h3>
              <h2 className="text-lg text-white font-medium title-font mb-4">
                Đất rừng phương nam
              </h2>
              <p className="leading-relaxed text-base">
                Fingerstache flexitarian street art 8-bit waistcoat. Distillery
                hexagon disrupt edison bulbche.
              </p>
            </div>
          </div>
          <div className="xl:w-1/4 md:w-1/2 p-4">
            <div className="bg-[#0b0e12]p-2 rounded-lg">
              <img
                className="h-40 rounded w-full object-cover object-center mb-6"
                src="https://chieuphimquocgia.com.vn/_next/image?url=https%3A%2F%2Fapi.chieuphimquocgia.com.vn%2FContent%2FImages%2F0017064_0.jpg&w=1920&q=75"
                alt="content"
              />
              <h3 className="tracking-widest text-indigo-500 text-xs font-medium title-font">
              Tâm lý tình cảm
              </h3>
              <h2 className="text-lg text-white font-medium title-font mb-4">
                Đất rừng phương nam
              </h2>
              <p className="leading-relaxed text-base">
                Fingerstache flexitarian street art 8-bit waistcoat. Distillery
                hexagon disrupt edison bulbche.
              </p>
            </div>
          </div>

        </div>
      </div>
    </section>
  );
};

export default ListMovie;
