
import { Footer, Header } from "components/ui"
// import MultipleRowSLick from "components/ui/MultipleRowSLick"
// import HomeCarousel from "pages/HomeCarousel"
import { Outlet } from "react-router-dom"


export const MainLayout = () => {
  return (
  
    <div className=" bg-[#10141b] ">
      <Header/>
      <div className="">
         <Outlet/>
      </div>
      <Footer/>

    </div>

  )
}

export default MainLayout