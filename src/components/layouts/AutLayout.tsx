
import { Footer, Header } from "components/ui"
import { useAuth } from "hooks"
import { Navigate, Outlet } from "react-router-dom"



export const AutLayout = () => {
  
  const {user} = useAuth()

  if(user) {
    return <Navigate to='/'/>
  }

  return (
    <div className="bg-[#10141b]">
        <Header/>
        <div className="h-screen w-screen relative pt-20 mb-10">
            <div className="absolute top-0 left-0 w-full h-full ">
            </div>
            <div className="absolute w-[450px] p-[30px] top-1/2 left-1/2 z-20 -translate-x-1/2 -translate-y-1/2 rounded-md">
                <Outlet />
            </div>
        </div>
        <Footer/>
    </div>
  )
}

export default AutLayout