export type Banner = {
    maBanner: number
    maPhim: number
    hinhAnh: string
}


export type Movie = {
    maPhim: number
    tenPhim: string
    biDanh: string
    trailer: string
    hinhAnh: string
    moTa: string
    maNhom: string
    ngayKhoiChieu: string
    danhGia: number
    hot: boolean
    dangChieu: boolean
    sapChieu: boolean
}

export type MoviePanigation = {
    currentPage: number
    count: number
    totalPages: number
    totalCount: number
    items : [
        {
            maPhim: number
            tenPhim: string
            biDanh: string
            trailer: string
            hinhAnh: string
            moTa: string
            maNhom: string
            ngayKhoiChieu: string
            danhGia: number
            hot: boolean
            dangChieu: boolean
            sapChieu: boolean
        }
    ]
    dateTime: string
    messageConstants: any

}

export type addMovie = {
    tenPhim: string,
    trailer: string,
    moTa: string,
    ngayKhoiChieu:string,
    dangChieu: boolean,
    sapChieu: boolean,
    hot: boolean,
    danhGia: number,
    hinhAnh: {}
}


export type editMovie = {
    maPhim: number
    tenPhim: string
    biDanh: string
    trailer: string
    hinhAnh: string
    moTa: string
    maNhom: string
    hot: boolean
    dangChieu: boolean
    sapChieu: boolean
    ngayKhoiChieu: string
    danhGia: number
}