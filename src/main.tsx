import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import "./index.css";
import "./assets/style.css"
import { BrowserRouter } from "react-router-dom";
import "./assets/style.css";
import { StyleProvider } from "@ant-design/cssinjs";

//thu vien slick
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

//redux
import { Provider } from "react-redux";
import { store } from "store";

//react-toastify
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <BrowserRouter>
    <Provider store={store}>
      <ToastContainer />
      <StyleProvider hashPriority="high">
        <App />
      </StyleProvider>
    </Provider>
  </BrowserRouter>
);
