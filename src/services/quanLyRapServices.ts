import { apiInstance } from "constant"
import { getCinemaSystem, getShowtime } from "types"

const api = apiInstance({
    baseURL: import.meta.env.VITE_QUAN_LY_RAP_API
})

export const quanLyRapServices = {
    getCinemaSystem: () => api.get<ApiResponse<getCinemaSystem>>('/LayThongTinLichChieuHeThongRap?maNhom=GP01'),
    getShowtime: (maPhim: any) => api.get<ApiResponse<getShowtime>>(`/LayThongTinLichChieuPhim?maPhim=${maPhim}`)
}