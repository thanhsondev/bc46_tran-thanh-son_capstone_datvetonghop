
import { apiInstance } from 'constant'
import { addMovie, editMovie } from 'types'

const api = apiInstance({
    baseURL: import.meta.env.VITE_QUAN_LY_PHIM_API
})
export const quanLyPhimServices = {
    getBanner: () => api.get<ApiResponse<any>>('/LayDanhSachBanner'),

    getMovieList: () =>  api.get<ApiResponse<any>>(`/LayDanhSachPhim`),

    addMovie: (payload: addMovie) => api.post<ApiResponse<addMovie>>('/ThemPhimUploadHinh', payload),
  
    editMovie: (maPhim: any) => api.get<ApiResponse<editMovie>>(`/LayThongTinPhim?MaPhim=${maPhim}`),

    updateMovie: (formData: any) => api.post<ApiResponse<any>>('/CapNhatPhimUpload', formData),

    deleteMovie: (maPhim: any) => api.post<ApiResponse<any>>(`/XoaPhim?=${maPhim}`)
}