import { apiInstance } from "constant"
import { bookingTypes, thongTinDatVe } from "types"



const api = apiInstance({
    baseURL: import.meta.env.VITE_QUAN_LY_DAT_VE_API
})

export const quanLyDatVeServices = {
    getBooking: (MaLichChieu: any) => api.get<ApiResponse<bookingTypes>>(`/LayDanhSachPhongVe?MaLichChieu=${MaLichChieu}`),
    datVe: (thongTinDatVe: any) => api.post<ApiResponse<thongTinDatVe>>('/DatVe', thongTinDatVe)
}