import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyRapServices } from "services";



export const getCinemaSystemThunk = createAsyncThunk('quanLyRap/getCinemaSystemThunk', async (_, {rejectWithValue}) => {
    try {
        const data = await quanLyRapServices.getCinemaSystem()
        return data.data.content
    } catch (err) {
        return rejectWithValue(err)
    }
})

export const getShowtimeThunk = createAsyncThunk('quanLyRap/getShowtimeThunk', async (id: any, {rejectWithValue}) => {
    try {
        const data = await quanLyRapServices.getShowtime(id)
        return data.data.content
    } catch (err) {
        return rejectWithValue(err)
    }
})
