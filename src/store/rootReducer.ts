import {combineReducers} from '@reduxjs/toolkit'
import { quanLyNguoiDungReducer } from './quanLyNguoiDung/slice'
import { quanLyPhimReducer } from './quanLyPhim/slice'
import { quanLyRapReducer } from './quanLyRap/slice'
import { quanLyDatVeReducer } from './quanLyDatVe/slice'



export const rootReducer = combineReducers({
    quanLyNguoiDungReducer,
    quanLyPhimReducer,
    quanLyRapReducer,
    quanLyDatVeReducer,
})