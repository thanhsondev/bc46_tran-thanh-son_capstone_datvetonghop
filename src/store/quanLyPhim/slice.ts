import { createSlice } from '@reduxjs/toolkit'
// import { Banner, Movie, addMovie, editMovie } from 'types';
import { addMovieThunk, deleteMovieThunk, editMovieThunk, getBannerMovieThunk, getMovieListThunk, updateMovieThunk } from './thunk';

type QuanlyPhimInitialState = {
  bannerMovie?: any[],
  movieList?: any[],
  movieDefault?: any,
  phimDangChieu?: any,
  phimSapChieu?: any,
  formData?: any,
  formEdit?: any,
  formDelete?: any
}

const initialState: QuanlyPhimInitialState = {
  bannerMovie: [],
  movieList:[],
  movieDefault: [],
  phimDangChieu:true,
  phimSapChieu:false,
  formData: {},
  formEdit: {},
  formDelete: []
}





const quanLyPhimSlice = createSlice({
  name: 'quanLyPhimSlice',
  initialState,
  reducers: {
    phimDangChieu: (state) => {
      state.phimDangChieu = !state.phimDangChieu
     state.movieList = state.movieDefault?.filter((movie: { dangChieu: any; })=> movie.dangChieu === state.phimDangChieu)
     state.phimSapChieu = false
    
    },
    phimSapChieu: (state) => {
      state.phimSapChieu = !state.phimSapChieu
      state.movieList = state.movieDefault?.filter((movie: { sapChieu: any; })=> movie.sapChieu === state.phimSapChieu)
      state.phimDangChieu = false
    }
  },
  extraReducers: (builder) => {
    builder.addCase(getMovieListThunk.fulfilled, (state, {payload}) => {
        state.movieList = payload
        state.movieDefault = state.movieList
    })
    .addCase(getBannerMovieThunk.fulfilled, (state, {payload}) => {
      state.bannerMovie = payload
    })
    .addCase(addMovieThunk.fulfilled, (state, {payload}) => {
      state.formData = payload
    } )
    .addCase(editMovieThunk.fulfilled, (state, {payload})=> {
      state.formEdit = payload
    })
    .addCase(updateMovieThunk.fulfilled, (state, {payload})=> {
      state.movieDefault = payload
    })
    .addCase(deleteMovieThunk.fulfilled, (state, {payload}) => {
      state.movieList = payload
    })
  }
});

export const {actions: quanLyPhimActions, reducer: quanLyPhimReducer} = quanLyPhimSlice

