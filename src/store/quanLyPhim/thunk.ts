import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyPhimServices } from "services";



export const getBannerMovieThunk = createAsyncThunk('quanLyPhim/getBannerMovieThunk', async (_, {rejectWithValue}) => {
    try {
        const data = await quanLyPhimServices.getBanner()
        return data.data.content
    } catch (err) {
      return  rejectWithValue(err)
    }
})

export const getMovieListThunk = createAsyncThunk('quanLyPhim/getMovieListThunk', async (_, {rejectWithValue}) => {
    try {
        const data = await quanLyPhimServices.getMovieList()
        return data.data.content
    } catch (err) {
      return  rejectWithValue(err)
    }
})


export const addMovieThunk = createAsyncThunk('quanLyPhim/addMovieThunk', async (payload: any, {rejectWithValue})=> {
  try {
    const accessToken = localStorage.getItem('accessToken')
    if(accessToken) {
      const data = await quanLyPhimServices.addMovie(payload)
      return data.data.content
    }
    return undefined
  } catch (error) {
    return rejectWithValue(error)
    
  }

})


export const editMovieThunk = createAsyncThunk('quanLyPhim/editMovieThunk', async (payload:any, {rejectWithValue})=> {
  try {
    const data = await quanLyPhimServices.editMovie(payload)
    return data.data.content
  } catch (error) {
    return rejectWithValue(error)
    
  }
})


export const updateMovieThunk = createAsyncThunk('quanLyPhim/updateMovieThunk', async (formData: any, {rejectWithValue})=> {
  try {
    const accessToken = localStorage.getItem("accessToken")
   
    if(accessToken) {
      const data = await quanLyPhimServices.updateMovie(formData)
      return data.data.content

    }
  } catch (error) {
    return rejectWithValue(error)
  }
})

export const deleteMovieThunk = createAsyncThunk('quanLyPhim/deleteMovieThunk', async (payload, {rejectWithValue})=> {
  try {
    const accessToken = localStorage.getItem("accessToken")
    if(accessToken) {
      const data = await quanLyPhimServices.deleteMovie(payload)
      return data.data.content

    }
  } catch (error) {
    return rejectWithValue(error)
  }
})