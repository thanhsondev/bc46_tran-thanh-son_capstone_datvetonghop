import { z } from "zod";



export const loginSchema = z.object({
    taiKhoan: z.string().nonempty('Vui lòng nhập tài khoản'),
    matKhau: z.string().nonempty('Vui lòng nhập mật khẩu')
})

export type loginSchemaTypes = z.infer<typeof loginSchema>